// ------------------------------------------------------------------------
// $RCSfile: TrackCmd.cpp,v $
// ------------------------------------------------------------------------
// $Revision: 1.1.1.1 $
// ------------------------------------------------------------------------
// Copyright: see Copyright.readme
// ------------------------------------------------------------------------
//
// Class: TrackCmd
//   The class for the OPAL TRACK command.
//
// ------------------------------------------------------------------------
//
// $Date: 2000/03/27 09:33:47 $
// $Author: Andreas Adelmann $
//
// ------------------------------------------------------------------------

#include "Track/TrackCmd.h"
#include "AbstractObjects/BeamSequence.h"
#include "AbstractObjects/OpalData.h"
#include "Attributes/Attributes.h"
#include "Structure/Beam.h"
#include "Track/Track.h"
#include "Track/TrackParser.h"
#include "Utilities/OpalException.h"

// Class Track
// ------------------------------------------------------------------------

namespace {

    // The attributes of class TrackRun.
    enum {
        LINE,         // The name of lattice to be tracked.
        BEAM,         // The name of beam to be used.
        DT,           // The integration timestep in second.
                      // In case of the adaptive integrator, time step guideline for
                      // external field integration.
        DTSCINIT,     // Only for adaptive integrator: Initial time step for space charge integration.
        DTAU,         // Only for adaptive integrator: Alternative way to set accuracy of space
                      // charge integration. Has no direct interpretation like DTSCINIT, but lower
                      // means smaller steps and more accurate. If given, DTSCINIT is not used. Useful
                      // for continuing with same step size in follow-up tracks.
        T0,           // The elapsed time (sec) of the bunch
        MAXSTEPS,     // The maximum timesteps we integrate
        ZSTART,       // Defines a z-location [m] where the reference particle starts
        ZSTOP,        // Defines a z-location [m], after which the simulation stops when the last particles passes
        STEPSPERTURN, // Return the timsteps per revolution period. ONLY available for OPAL-cycl.
        TIMEINTEGRATOR, // the name of time integrator
        MAP_ORDER,    // Truncation order of maps for ThickTracker (default: 1 (linear))
        SIZE
    };
}

TrackCmd::TrackCmd():
    Action(SIZE, "TRACK",
           "The \"TRACK\" command initiates tracking.") {
    itsAttr[LINE] = Attributes::makeString
                    ("LINE", "Name of lattice to be tracked");
    itsAttr[BEAM] = Attributes::makeString
                    ("BEAM", "Name of beam to be used", "UNNAMED_BEAM");
    itsAttr[DT] = Attributes::makeRealArray
                  ("DT", "The integration timestep in seconds");
    itsAttr[DTSCINIT] = Attributes::makeReal
                  ("DTSCINIT", "Only for adaptive integrator: Initial time step for space charge integration", 1e-12);
    itsAttr[DTAU] = Attributes::makeReal
                  ("DTAU", "Only for adaptive integrator: Alternative way to set accuracy of space integration.", -1.0);
    itsAttr[T0] = Attributes::makeReal
                  ("T0", "The elapsed time of the bunch in seconds", 0.0);
    itsAttr[MAXSTEPS] = Attributes::makeRealArray
                        ("MAXSTEPS", "The maximum number of integration steps dt, should be larger ZSTOP/(beta*c average)");
    itsAttr[ZSTART] = Attributes::makeReal
                      ("ZSTART", "Defines a z-location [m] where the reference particle starts", 0.0);
    itsAttr[ZSTOP] = Attributes::makeRealArray
                     ("ZSTOP", "Defines a z-location [m], after which the simulation stops when the last particles passes");
    itsAttr[STEPSPERTURN] = Attributes::makeReal
                            ("STEPSPERTURN", "The time steps per revolution period, only for opal-cycl.", 720);
    itsAttr[TIMEINTEGRATOR] = Attributes::makePredefinedString
        ("TIMEINTEGRATOR", "Name of time integrator to be used", {"RK-4", "RK_4", "LF-2", "LF_2", "MTS"}, "RK_4");

    itsAttr[MAP_ORDER] = Attributes::makeReal
                     ("MAP_ORDER", "Truncation order of maps for ThickTracker (default: 1, i.e. linear)", 1);

    registerOwnership(AttributeHandler::COMMAND);
    AttributeHandler::addAttributeOwner("TRACK", AttributeHandler::COMMAND, "RUN");
    AttributeHandler::addAttributeOwner("TRACK", AttributeHandler::COMMAND, "ENDTRACK");
}

TrackCmd::TrackCmd(const std::string &name, TrackCmd *parent):
    Action(name, parent)
{}


TrackCmd::~TrackCmd()
{}


TrackCmd *TrackCmd::clone(const std::string &name) {
    return new TrackCmd(name, this);
}

std::vector<double> TrackCmd::getDT() const {
    std::vector<double> dTs = Attributes::getRealArray(itsAttr[DT]);
    if (dTs.size() == 0) {
        dTs.push_back(1e-12);
    }
    for (double dt : dTs) {
        if (dt < 0.0) {
            throw OpalException("TrackCmd::getDT",
                                "The time steps provided with DT have to be positive");
        }
    }
    return dTs;
}

double TrackCmd::getDTSCINIT() const {
    return Attributes::getReal(itsAttr[DTSCINIT]);
}

double TrackCmd::getDTAU() const {
    return Attributes::getReal(itsAttr[DTAU]);
}

double TrackCmd::getT0() const {
    return Attributes::getReal(itsAttr[T0]);
}

double TrackCmd::getZSTART() const {
    double zstart = Attributes::getReal(itsAttr[ZSTART]);
    return zstart;
}

std::vector<double> TrackCmd::getZSTOP() const {
    std::vector<double> zstop = Attributes::getRealArray(itsAttr[ZSTOP]);
    if (zstop.size() == 0) {
        zstop.push_back(1000000.0);
    }
    return zstop;
}

std::vector<unsigned long long> TrackCmd::getMAXSTEPS() const {
    std::vector<double> maxsteps_d = Attributes::getRealArray(itsAttr[MAXSTEPS]);
    std::vector<unsigned long long> maxsteps_i;
    if (maxsteps_d.size() == 0) {
        maxsteps_i.push_back(10ul);
    }
    for (double numSteps : maxsteps_d) {
        if (numSteps < 0) {
            throw OpalException("TrackCmd::getMAXSTEPS",
                                "The number of steps provided with MAXSTEPS has to be positive");
        } else {
            unsigned long long value = numSteps;
            maxsteps_i.push_back(value);
        }
    }

    return maxsteps_i;
}

int TrackCmd::getSTEPSPERTURN() const {
    return (int) Attributes::getReal(itsAttr[STEPSPERTURN]);
}

// return int type rathor than string to improve the speed
int TrackCmd::getTIMEINTEGRATOR() const {
    std::string name = Attributes::getString(itsAttr[TIMEINTEGRATOR]);
    int  nameID;
    if(name == std::string("RK-4") || name == std::string("RK_4"))
        nameID =  0;
    else if(name == std::string("LF-2") || name == std::string("LF_2"))
        nameID =  1;
    else if(name == std::string("MTS"))
        nameID = 2;
    else if(name == std::string("AMTS"))
        nameID = 3;
    else
        nameID = -1;

    return nameID;
}

void TrackCmd::execute() {
    // Find BeamSequence and Beam definitions.
    BeamSequence *use = BeamSequence::find(Attributes::getString(itsAttr[LINE]));
    Beam *beam = Beam::find(Attributes::getString(itsAttr[BEAM]));

    std::vector<double> dt = getDT();
    double t0 = getT0();
    std::vector<unsigned long long> maxsteps = getMAXSTEPS();
    int    stepsperturn = getSTEPSPERTURN();
    double zstart = getZSTART();
    std::vector<double> zstop = getZSTOP();
    int timeintegrator = getTIMEINTEGRATOR();

    size_t numTracks = dt.size();
    numTracks = std::max(numTracks, maxsteps.size());
    numTracks = std::max(numTracks, zstop.size());
    for (size_t i = dt.size(); i < numTracks; ++ i) {
        dt.push_back(dt.back());
    }
    for (size_t i = maxsteps.size(); i < numTracks; ++ i) {
        maxsteps.push_back(maxsteps.back());
    }
    for (size_t i = zstop.size(); i < numTracks; ++ i) {
        zstop.push_back(zstop.back());
    }

   // Execute track block.
    Track::block = new Track(use, beam->getReference(), dt, maxsteps,
                             stepsperturn, zstart, zstop,
                             timeintegrator, t0, getDTSCINIT(), getDTAU());

    Track::block->truncOrder = (int)Attributes::getReal(itsAttr[MAP_ORDER]);

    Track::block->parser.run();

    // Clean up.
    delete Track::block;
    Track::block = 0;
}